#!/usr/bin/env python2.7
import collections
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as md
from datetime import datetime
from optparse import OptionParser


parser = OptionParser()
parser.add_option("-r",  "--read", dest="datafile", help="Read file")
parser.add_option("-s", "--seconds", dest="ppsec", action="store_true",
    help="Generate packets per second graph", default=False)
parser.add_option("-m", "--minute", dest="ppmin", action="store_true",
    help="Generate packets per minute graph", default=False)
(options, args) = parser.parse_args()
exitflag = False
if options.datafile == None:
    print "Must specify source file"
    exitflag = True

if options.ppsec is False and options.ppmin is False:
    print "At least one desired graphic mus be specified"
    exitflag = True

if exitflag is True:
    print "Usage:", sys.argv[0], "-r sourcefile.txt -m"
    print sys.argv[0], "-h for help"
    print ""
    exit()

capfile = open(options.datafile, 'r')

lines = capfile.readlines()

if options.ppsec is True:
    secs = []
    slddat = []
    sldval = []
    for line in lines:
        secs.append(line[:19])
    secdict = collections.Counter(secs)
    osecdict = collections.OrderedDict(sorted(secdict.items()))
    for f1 in osecdict.keys():
        slddat.append(datetime.strptime(f1, '%Y-%m-%d %H:%M:%S'))
        sldval.append(float(osecdict[f1]))
    saddat = np.array(slddat)
    sadval = np.array(sldval)
    fig = plt.figure("Packets per second")
    ax = plt.gca()
    xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
    ax.xaxis.set_major_formatter(xfmt)
    ax = fig.add_subplot(111)
    ax.plot(saddat, sadval, 'o-')
    fig.autofmt_xdate()

if options.ppmin is True:
    mins = []
    mlddat = []
    mldval = []
    for line in lines:
        mins.append(line[:16])
    mindict = collections.Counter(mins)
    omindict = collections.OrderedDict(sorted(mindict.items()))
    for f2 in omindict.keys():
        mlddat.append(datetime.strptime(f2, '%Y-%m-%d %H:%M'))
        mldval.append(float(omindict[f2]))
    maddat = np.array(mlddat)
    madval = np.array(mldval)
    figm = plt.figure("Packets per minute")
    ax = plt.gca()
    xfmtm = md.DateFormatter('%Y-%m-%d %H:%M')
    ax.xaxis.set_major_formatter(xfmtm)
    ax = figm.add_subplot(111)
    ax.plot(maddat, madval, 'o-')
    figm.autofmt_xdate()


#fig.show()
plt.show()
